# Score Keeper Kata

See [KataLog](http://kata-log.rocks/score-keeper-kata)

## Problem
We need software to deliver the proper data to the scoreboard for a basketball team. Unfortunately the people using our software are not the brightest lights under the sun, so they need six buttons (each team can score either 1, 2 or 3 points with a single shot).

## Your Task
Write a class `ScoreKeeper` which offers following methods:

```sh
void scoreTeamA1()
void scoreTeamA2()
void scoreTeamA3()
void scoreTeamB1()
void scoreTeamB2()
void scoreTeamB3()
String getScore()
```
## Rules
The returned String always has seven characters. An example would be `000:000`

# Install

### Get the code
```bash
git clone git@bitbucket.org:devmultihelpers/score-keeper-kata.git {{FOLDERNAME}}
```
### Kata docker environment installation
```bash
docker pull multihelpers/katas
```
### Kata docker environment remove
```bash
docker rm -f katas
```
### Kata docker environment start
```bash
docker run -dit --name katas -v /home/{{USERNAME}}/code/multihelpers/{{DIRNAME}}:/var/www -p 8861:80 multihelpers/katas
```
### Kata docker container enter
```bash
docker exec -it katas bash
```
### Kata dependencies install
From inside docker container and located at root source code...
```bash
composer install
```
### Kata commands
```bash    
./vendor/bin/phpunit 
./vendor/bin/phpunit --coverage-text
```
