<?php

namespace tddPhpSkeleton\Tests;

use tddPhpSkeleton\KataSource;

class KataTest extends \PHPUnit_Framework_TestCase
{
    protected   $source;

    protected function setUp()
    {
        $this->source = new KataSource();
    }

    public function testEnv()
    {
        $result = $this->source->env();

        $this->AssertTrue($result);
    }
}

